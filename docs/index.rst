.. BasicTools documentation master file, created by
   sphinx-quickstart on Tue Feb 20 11:03:47 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BasicTools's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 4

   _source/BasicTools

.. literalinclude:: ../README.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
