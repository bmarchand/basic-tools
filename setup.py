# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
#
import os, sys 
from setuptools import setup, find_packages


requirements = ["numpy", "sympy" ] 

### ENvironement variable to specify if cython part should be considered or not
### export BASICTOOLS_BUILD_CYTHON=True 
### or 
### export BASICTOOLS_BUILD_CYTHON=False

BASICTOOLS_BUILD_CYTHON=os.getenv( "BASICTOOLS_BUILD_CYTHON", "True")

if BASICTOOLS_BUILD_CYTHON=="True":
    try: 
        from Cython.Build import cythonize
    except:
        sys.exit("Cython not available on your install, install it or set environment variable BASICTOOLS_BUILD_CYTHON to False")
    import numpy, os

    enable_MKL = True
    debug = False
    force = True # to force recompilation
    annotate = debug # to generate annotation (html files )
    useOpenmp = True

    if debug:
        compile_args = ['-g','-O', '-std=c++11']
    else:
        compile_args = ['-O2', '-std=c++11']

    if useOpenmp:
        compile_args.append("-openmp")

    if enable_MKL:
        #compile_args.append("-DEIGEN_USE_MKL_ALL")
        compile_args.append("-DMKL_DIRECT_CALL")

    include_path = [numpy.get_include(), os.environ['EIGEN_INC']]
    modules = cythonize("src/BasicTools/FE/*.pyx", gdb_debug=debug, annotate=annotate, include_path=include_path, force=force)

    for m in modules:
        m.include_dirs = include_path + ["src/BasicTools"]
        m.extra_compile_args = compile_args
        if enable_MKL:
            m.extra_link_args.append("-lmkl_core")
            m.extra_link_args.append("-lmkl_avx")
            m.extra_link_args.append("-lmkl_intel_lp64")
            m.extra_link_args.append("-lmkl_sequential")
            m.extra_link_args.append("-lmkl_def")

else:
    modules = []
    

setup(name='BasicTools',
      packages=find_packages('src'),
      ext_modules=modules,
      install_requires=requirements, 
      package_dir={'': 'src'})
